# Docker Library

Copyright 2019 Kulado Inc. 
eric@kulado.com


## Description

This is the Git repo of docker images maintained by Geeks Accelerator. 


## Push updates

```bash
docker build -t golang1.12-docker golang/1.12/docker
docker tag golang1.12-docker geeksaccelerator/docker-library:golang1.12-docker
docker push geeksaccelerator/docker-library:golang1.12-docker
```

## Join us on Gopher Slack

If you are having problems installing, troubles getting the project running or would like to contribute, join the channel #saas-starter-kit on [Gopher Slack](http://invite.slack.golangbridge.org/) 
